<?php

class Instagram {
    private static $ch;
    private static $ua;

    public function __construct() {
        static::$ch = curl_init();
        static::$ua = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) ' .
             'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102' .
             'Safari/537.36';
    }

    /**
     * @param: string $profile IG profile URL
     * @returns: array Either empty array or 9 latest images
     */
    public function get_feed($profile) {
        curl_setopt(static::$ch, CURLOPT_URL, $profile);
        curl_setopt(static::$ch, CURLOPT_USERAGENT, static::$ua);
        curl_setopt(static::$ch, CURLOPT_HEADER, 0);
        curl_setopt(static::$ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(static::$ch, CURLOPT_TIMEOUT, 10);

        $ig = curl_exec(static::$ch);
        preg_match('#window._sharedData = (.*?)</script>#s', $ig, $matches);
        if (count($matches) != 2) {
            return [];
        } else {
            $data = json_decode(rtrim($matches[1], ';'), true);
            foreach ($data['entry_data']['ProfilePage'] as $value) {
                foreach ($value as $k => $v) {
                    if ($k == 'user') {
                        $nodes = $v['media']['nodes'];
                        break;
                    }
                }
            }

            if (! isset($nodes)) {
                return [];
            }

            $media = array();
            foreach ($nodes as $node) {
                $media[] = array('title' => $node['caption'],
                   'code' => $node['code'],
                   'url' => $node['display_src'],
                   'link' => $profile,
                   'date' => $node['date']);
            }

            return $media;
        }
    }
}


class Rss {
    public function __construct($profiles) {
        $instagram = new Instagram;
        $feeds = array();
        foreach ($profiles as $profile) {
            $feeds = array_merge($feeds, $instagram->get_feed($profile));
        }

        usort($feeds, function($a, $b) {
            return $b['date'] - $a['date'];
        });

        $items = '';
        foreach ($feeds as $media) {
            $date = gmdate(DATE_RSS, $media['date']);
            $guid = 'https://www.instagram.com/p/' . $media['code'] . '/';
            $items .= "<item><title>{$media['title']}</title><link>$guid</link>" .
                "<description>&lt;img src=\"{$media['url']}\" /&gt;</description>" .
                "<guid>$guid</guid><pubDate>$date</pubDate></item>";
        }

        $date = gmdate(DATE_RSS, $feeds[0]['date']);
        $rss = "<rss version=\"2.0\"
            xmlns:content=\"http://purl.org/rss/1.0/modules/content/\"
            xmlns:wfw=\"http://wellformedweb.org/CommentAPI/\"
            xmlns:dc=\"http://purl.org/dc/elements/1.1/\"
            xmlns:sy=\"http://purl.org/rss/1.0/modules/syndication/\"
            xmlns:slash=\"http://purl.org/rss/1.0/modules/slash/\" >
            <channel>
                <title>Mellis Berry</title>
                <link>https://mellisberry.com</link>
                <description>Berry infused honey delights from producer</description>
                <lastBuildDate>$date</lastBuildDate>
                $items
            </channel>
        </rss>";

        header('Content-Type: application/rss+xml; charset=utf-8');
        echo '<?xml version="1.0" encoding="UTF-8"?>';
        echo $rss;
    }

}
